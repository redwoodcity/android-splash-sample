package jp.redwoodcity.splashsample;

import android.app.Activity;
import android.os.Bundle;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// splash.xmlをViewに指定します。
		setContentView(R.layout.activity_main);
	}
}
